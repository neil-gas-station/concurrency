package org.concurrency.chapter8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

public class ThreadPoolTestHarness {
    public long timeTasks(int threadCount, int timeoutTimeInSeconds, boolean shouldStartAllThreads, final Runnable task) {
        List<Future> runningTasks = new ArrayList<>();
        final Thread timeoutCheckingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int timeCountDown = timeoutTimeInSeconds;
                while(!runningTasks.isEmpty()) {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                        timeCountDown--;
                        Iterator<Future> taskIterator = runningTasks.listIterator();
                        while(taskIterator.hasNext()) {
                            Future task = taskIterator.next();
                            if(task.isDone() || task.isCancelled()) {
                                taskIterator.remove();
                            } else if(timeCountDown <= 0){
                                task.cancel(true);
                            }
                        }
                    } catch (InterruptedException ignore) {
                    }
                }
            }
        });

        AtomicReference<Long> startTime = new AtomicReference<Long>(0l);
        final CyclicBarrier startBarrier = new CyclicBarrier(threadCount, ()-> {
            startTime.set(System.nanoTime());
            timeoutCheckingThread.start();
        });

        final CyclicBarrier endBarrier = new CyclicBarrier(threadCount+1);


        ThreadPoolExecutor taskExecuter = new ExtendedTimingThreadPool(threadCount, threadCount*2, 10, TimeUnit.SECONDS,shouldStartAllThreads, new LinkedBlockingDeque<Runnable>());
        try {
            for (int i = 0; i < threadCount; i++) {
                Future runningTask = taskExecuter.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            startBarrier.await();
                            try {
                                task.run();
                            } finally {
                                endBarrier.await();
                            }
                        } catch (InterruptedException | BrokenBarrierException ignore) {
                        }
                    }
                });
                runningTasks.add(runningTask);
            }

            endBarrier.await();
        } catch (InterruptedException | BrokenBarrierException ignore) {
        }finally {
            taskExecuter.shutdown();
        }
        return System.nanoTime() - startTime.get();
    }
}
