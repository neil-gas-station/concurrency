package org.concurrency.chapter8;

import java.util.concurrent.*;

public class ExtendedTimingThreadPool extends ThreadPoolExecutor {
    public ExtendedTimingThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, boolean shouldStartAllCoreThreads, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        if(shouldStartAllCoreThreads) {
            initThreads();
        } else {
            System.out.println("Thread pool initialized, pool size is: " + this.getPoolSize());
        }
    }

    private void initThreads() {
        final CyclicBarrier startBarrier = new CyclicBarrier(this.getCorePoolSize() + 1, ()-> {
            System.out.println("Thread pool initialized, pool size is: " + this.getPoolSize());
        });

        final Runnable initCoreThreadPoolTask = new Runnable() {
            @Override
            public void run() {
                try {
                    startBarrier.await();
                } catch (InterruptedException | BrokenBarrierException ignore) {
                }
            }
        };

        for(int i=0;i<this.getCorePoolSize();i++) {
            this.execute(initCoreThreadPoolTask);
        }
        try {
            startBarrier.await();
        } catch (InterruptedException | BrokenBarrierException ignore) {
        }
    }
}
