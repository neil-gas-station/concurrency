package org.concurrency.chapter3;

import java.util.HashMap;
import java.util.Map;

public class ImprovedHashMap<K, V> {
    private final Map<K, V> innerMap = new HashMap();

    public synchronized void put(K key, V value) {
        innerMap.put(key,value);
    }

    public synchronized int size() {
        return innerMap.size();
    }
}
