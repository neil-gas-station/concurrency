package org.concurrency.chapter1;

import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class UnThreadSafeSequence {
    private int value;

    public int getSequence() {
        return value++;
    }

    public int getValue() {
        return value;
    }
}
