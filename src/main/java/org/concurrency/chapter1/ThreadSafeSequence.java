package org.concurrency.chapter1;

public class ThreadSafeSequence {
    private int value;

    public synchronized int getSequence() {
        return value++;
    }

    public int getValue() {
        return value;
    }
}
