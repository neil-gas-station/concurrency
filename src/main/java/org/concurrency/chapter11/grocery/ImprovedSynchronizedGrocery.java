package org.concurrency.chapter11.grocery;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class ImprovedSynchronizedGrocery implements Grocery{
    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    private final ReentrantLock fruitLock = new ReentrantLock();

    private final ReentrantLock vegetableLock = new ReentrantLock();

    @Override
    public void addFruit(int index, String fruit) {
        fruitLock.lock();
        try{
            fruits.add(index, fruit);
        }finally {
            fruitLock.unlock();
        }
    }

    @Override
    public void addVegetable(int index, String vegetable) {
        vegetableLock.lock();
        try{
            vegetables.add(index, vegetable);
        }finally {
            vegetableLock.unlock();
        }
    }

    public int getFruitSize() {
        return this.fruits.size();
    }

    public int getVegetableSize() {
        return this.vegetables.size();
    }
}
