package org.concurrency.chapter11.grocery;

public interface Grocery {
    void addFruit(int index, String fruit);
    void addVegetable(int index, String vegetable);
}
