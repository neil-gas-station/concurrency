package org.concurrency.chapter6;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

public class ImprovedTestHarness {
    public long timeTasks(int threadCount, final Runnable task) throws InterruptedException, BrokenBarrierException {
        AtomicReference<Long> startTime = new AtomicReference<Long>(0l);
        final CyclicBarrier startBarrier = new CyclicBarrier(threadCount, ()-> {
            startTime.set(System.nanoTime());
        });

        final CyclicBarrier endBarrier = new CyclicBarrier(threadCount+1);

        ExecutorService executer = Executors.newFixedThreadPool(threadCount);
        for (int i = 0; i < threadCount; i++) {
            executer.submit(() -> {
                try {
                    startBarrier.await();
                    try {
                        task.run();
                    } finally {
                        endBarrier.await();
                    }
                } catch (Exception ignore) {
                }
            });
        }

        endBarrier.await();
        return System.nanoTime() - startTime.get();
    }
}
