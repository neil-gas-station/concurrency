package org.concurrency.chapter2;

public class SafeStatesByNoEscape {
    private String[] states;

     private static ThreadLocal<SafeStatesByNoEscape> stateContext = new ThreadLocal<>();

    private SafeStatesByNoEscape() {
        states = new String[] {"AK","AL","AM"};
    }

    public String getValue(int index) {
        return states[index];
    }

    public void setValue(String value, int index){
        if(index<states.length && index>=0) {
            states[index] = value;
        }
    }

    public static SafeStatesByNoEscape newInstance() {
        SafeStatesByNoEscape safeStatesByNoEscape = stateContext.get();
        if(null == safeStatesByNoEscape) {
            stateContext.set(new SafeStatesByNoEscape());
            safeStatesByNoEscape = stateContext.get();
        }
        return safeStatesByNoEscape;
    }
}
