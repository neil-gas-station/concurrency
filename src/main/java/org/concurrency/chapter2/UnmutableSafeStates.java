package org.concurrency.chapter2;

public class UnmutableSafeStates {
    private final String[] states = new String[] {"AK","AL","AM"};

    public String[] getStates() {
        return states;
    }
}
