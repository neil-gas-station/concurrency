package org.concurrency.chapter13;

import org.concurrency.chapter11.grocery.Grocery;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizedGroceryWithFairLock implements Grocery {
    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();

    private final ReentrantLock fruitLock = new ReentrantLock(true);

    private final ReentrantLock vegetableLock = new ReentrantLock(true);

    @Override
    public void addFruit(int index, String fruit){
        fruitLock.lock();
        try{
            fruits.add(index, fruit);
            Thread.sleep(10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            fruitLock.unlock();
        }
    }

    @Override
    public void addVegetable(int index, String vegetable) {
        vegetableLock.lock();
        try{
            vegetables.add(index, vegetable);
            Thread.sleep(10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            vegetableLock.unlock();
        }
    }

}
