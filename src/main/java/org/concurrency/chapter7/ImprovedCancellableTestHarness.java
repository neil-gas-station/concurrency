package org.concurrency.chapter7;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

public class ImprovedCancellableTestHarness {
    private List<Future> runningTasks = new ArrayList<>();

    private volatile int timeoutTimeInSeconds = 0;

    private final Thread timeoutCheckingThread = new Thread(new Runnable() {
        @Override
        public void run() {
            while(!runningTasks.isEmpty()) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                    timeoutTimeInSeconds--;
                    Iterator<Future> taskIterator = runningTasks.listIterator();
                    while(taskIterator.hasNext()) {
                        Future task = taskIterator.next();
                        if(task.isDone() || task.isCancelled()) {
                            taskIterator.remove();
                        } else if(timeoutTimeInSeconds <= 0){
                            task.cancel(true);
                        }
                    }
                } catch (InterruptedException ignore) {
                }
            }
        }
    });
    public long timeTasks(int threadCount, int timeoutInSeconds, final Runnable task) {
        this.timeoutTimeInSeconds = timeoutInSeconds;

        AtomicReference<Long> startTime = new AtomicReference<Long>(0l);
        final CyclicBarrier startBarrier = new CyclicBarrier(threadCount, ()-> {
            startTime.set(System.nanoTime());
            timeoutCheckingThread.start();
        });

        final CyclicBarrier endBarrier = new CyclicBarrier(threadCount+1);


        ExecutorService taskExecuter = Executors.newFixedThreadPool(threadCount);
        try {
            for (int i = 0; i < threadCount; i++) {
               Future runningTask = taskExecuter.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            startBarrier.await();
                            try {
                                task.run();
                            } finally {
                                endBarrier.await();
                            }
                        } catch (InterruptedException | BrokenBarrierException ignore) {
                        }
                    }
                });
                runningTasks.add(runningTask);
            }

            endBarrier.await();
        } catch (InterruptedException | BrokenBarrierException ignore) {
        }finally {
            taskExecuter.shutdown();
        }
        return System.nanoTime() - startTime.get();
    }
}
