package org.concurrency.chapter9;

public class LockOrderingDeadlock {
    private final Object left = new Object();
    private final Object right = new Object();

    public void leftRight() throws InterruptedException {
        synchronized (left) {
            System.out.println("I got the left lock.");
            Thread.sleep(3000);
            System.out.println("Now I'm trying to get the right lock....");
            synchronized (right) {
                System.out.println("I got the right lock at the end.");
            }
        }
    }

    public void rightLeft() throws InterruptedException {
        synchronized (right) {
            System.out.println("I got the right lock.");
            Thread.sleep(3000);
            System.out.println("Now I'm trying to get the left lock....");
            synchronized (left) {
                System.out.println("I got the left lock at the end.");
            }
        }
    }
}
