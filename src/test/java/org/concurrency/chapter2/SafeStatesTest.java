package org.concurrency.chapter2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

public class SafeStatesTest {
    @Test
    public void cannotBeChangedByOtherThread_IfThereIsNoEscape() throws ExecutionException, InterruptedException {
        SafeStatesByNoEscape safeStatesByNoEscape = SafeStatesByNoEscape.newInstance();

        Callable<String> callableTask = () -> {
            SafeStatesByNoEscape safeStatesByNoEscapeInThread = SafeStatesByNoEscape.newInstance();
           safeStatesByNoEscapeInThread.setValue("neren",0);
            return safeStatesByNoEscapeInThread.getValue(0);
        };

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(callableTask);

        String changedValue = future.get();
        Assertions.assertFalse(safeStatesByNoEscape.getValue(0).equals(changedValue));
    }
}
