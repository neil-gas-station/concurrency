package org.concurrency.chapter2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

public class UnSafeStatesTest {


    @Test
    public void canBeChanged_WhenChangeTheStatesOfUnSafeStatesInASubThread() throws ExecutionException, InterruptedException {
        UnSafeStates unSafeStates = new UnSafeStates();

        Callable<String> callableTask = () -> {
            String[] escapedStrings = unSafeStates.getStates();
            escapedStrings[0] = "neren";
            return escapedStrings[0];
        };

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(callableTask);

        String changedValue = future.get();
        Assertions.assertTrue(unSafeStates.getStates()[0].equals(changedValue));
    }

    @Test
    public void stillCanBeChanged_IfTheVaribleIsSetUnmutable() throws ExecutionException, InterruptedException {

        UnmutableSafeStates unmutableSafeStates = new UnmutableSafeStates();
        Callable<String> callableTask = () -> {
            String[] escapedStrings = unmutableSafeStates.getStates();
            escapedStrings[0] = "neren";
            return escapedStrings[0];
        };

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(callableTask);

        String changedValue = future.get();
        Assertions.assertTrue(unmutableSafeStates.getStates()[0].equals(changedValue));

    }


}
