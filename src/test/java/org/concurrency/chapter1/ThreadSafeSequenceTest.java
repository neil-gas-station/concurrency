package org.concurrency.chapter1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class ThreadSafeSequenceTest {
    private ThreadSafeSequence threadSafeSequence;

    @BeforeEach
    public void setup() {
        this.threadSafeSequence = new ThreadSafeSequence();
    }

    @Test
    public void threadSafeSequenceShouldBeRight_whenCalledInMultipleThreads() {
        List<Integer> numberOfThreads = Arrays.asList(1,2,3,4);
        numberOfThreads.parallelStream().forEach(number-> {
            for(int i=0;i<500;i++) {
                this.threadSafeSequence.getSequence();
                try {
                    Thread.sleep(100l);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        System.out.println("Get sequence 500 times in 4 threads, the final value expected to be: 2000, the real result is: " + this.threadSafeSequence.getValue());
        Assertions.assertTrue(this.threadSafeSequence.getValue() == 2000);
    }
}
