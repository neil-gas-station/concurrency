package org.concurrency.chapter1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class UnThreadSafeSequenceTest {

    private UnThreadSafeSequence  unThreadSafeSequence;

    @BeforeEach
    public void setup() {
        this.unThreadSafeSequence =  new UnThreadSafeSequence();
    }

    @Test
    public void unThreadSafeSequenceShouldBeWrong_whenCalledInMultipleThreads() {
        List<Integer> numberOfThreads = Arrays.asList(1,2,3,4);
        numberOfThreads.parallelStream().forEach(number-> {
            for(int i=0;i<500;i++) {
                this.unThreadSafeSequence.getSequence();
                try {
                    Thread.sleep(100l);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        System.out.println("Get sequence 500 times in 4 threads, the final value expected to be: 2000, the real result is: " + this.unThreadSafeSequence.getValue());
        Assertions.assertTrue(this.unThreadSafeSequence.getValue() != 2000);
    }
}
