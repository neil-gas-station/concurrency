package org.concurrency.chapter6;

import org.concurrency.chapter3.ImprovedHashMap;
import org.concurrency.chapter5.TestHarness;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentMapPerformanceV2Test {
    private final int THREAD_COUNT = 50;

    private final int ADD_VALUE_TIMES = 100;

    private final String KEY_PREFIX = "performanceTest";

    private final Map<Object, Object> synchronizedMap = Collections.synchronizedMap(new HashMap<>());
    private final Runnable  synchronizedMapTestTask= new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                synchronizedMap.put(KEY_PREFIX + i, i);
            }
        }
    };

    private final Map<Object, Object> concurrentHashMap = new ConcurrentHashMap<>();
    private final Runnable concurrentHashMapTestTask = new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                concurrentHashMap.put(KEY_PREFIX + i, i);
            }
        }
    };

    private final ImprovedHashMap<Object, Object> improvedHashMap = new ImprovedHashMap<>();
    private final Runnable improvedHashMapTestTask = new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                improvedHashMap.put(KEY_PREFIX + i, i);
            }
        }
    };

    @Test
    public void concurrentMapPerformance_whenAddValue1000000TimesWith20Threads() throws InterruptedException, BrokenBarrierException {

        TestHarness testHarness = new TestHarness();
        ImprovedTestHarness improvedTestHarness = new ImprovedTestHarness();

        long timeOfSynchronizedMap = testHarness.timeTasks(THREAD_COUNT, synchronizedMapTestTask);
        long timeOfSynchronizedMapAfterImproved = improvedTestHarness.timeTasks(THREAD_COUNT, synchronizedMapTestTask);
        System.out.println(String.format(
                "Time(Nano Second) used to add %s times with %s threads concurrently using Synchronized HashMap.\n TestHarness:         %s \n ImprovedTestHarness: %s.\n\n\n\n",
                ADD_VALUE_TIMES,
                THREAD_COUNT,
                timeOfSynchronizedMap,
                timeOfSynchronizedMapAfterImproved));
        Assertions.assertTrue(timeOfSynchronizedMapAfterImproved < timeOfSynchronizedMap);

        long timeOfConcurrentHashMap = testHarness.timeTasks(THREAD_COUNT, concurrentHashMapTestTask);
        long timeOfConcurrentHashMapImproved = improvedTestHarness.timeTasks(THREAD_COUNT, concurrentHashMapTestTask);
        System.out.println(String.format(
                "Time(Nano Second) used to add %s times with %s threads concurrently using Concurrent HashMap.\n TestHarness:         %s \n ImprovedTestHarness: %s.\n\n\n\n",
                ADD_VALUE_TIMES,
                THREAD_COUNT,
                timeOfConcurrentHashMap,
                timeOfConcurrentHashMapImproved));
        Assertions.assertTrue(timeOfConcurrentHashMapImproved < timeOfConcurrentHashMap);

        long timeOfImprovedHashMap = testHarness.timeTasks(THREAD_COUNT, improvedHashMapTestTask);
        long timeOfImprovedHashMapImproved = improvedTestHarness.timeTasks(THREAD_COUNT, improvedHashMapTestTask);
        System.out.println(String.format(
                "Time(Nano Second) used to add %s times with %s threads concurrently using Improved HashMap.\n TestHarness:         %s \n ImprovedTestHarness: %s. \n\n\n\n",
                ADD_VALUE_TIMES,
                THREAD_COUNT,
                timeOfImprovedHashMap,
                timeOfImprovedHashMapImproved));
        Assertions.assertTrue(timeOfImprovedHashMapImproved < timeOfImprovedHashMap);
    }
}
