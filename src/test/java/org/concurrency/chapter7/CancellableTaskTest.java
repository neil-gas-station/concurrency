package org.concurrency.chapter7;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class CancellableTaskTest {
    private final int THREAD_COUNT = 20;

    private final int ADD_VALUE_TIMES = 100;

    private final String KEY_PREFIX = "cancellableTaskTest";


    private final Map<Object, Object> concurrentHashMap = new ConcurrentHashMap<>();
    private final Runnable concurrentHashMapTestTask = new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                concurrentHashMap.put(KEY_PREFIX + i, i);
                int sleepInMilliseconds = (new Random()).nextInt(100);
                try {
                    Thread.sleep(sleepInMilliseconds);
                } catch (InterruptedException e) {
                    System.out.println("I didn't finish my task in given time.");
                    throw new RuntimeException(e);
                }
            }
            System.out.println("I finished my task in given time.");
        }
    };
    @Test
    public void timeoutTasksShouldBeCancelled_whenTasksAreTimeout() {

        ImprovedCancellableTestHarness testHarness = new ImprovedCancellableTestHarness();

        long timeOfConcurrentHashMap = testHarness.timeTasks(THREAD_COUNT, 5,concurrentHashMapTestTask);

        System.out.println("The total time consumed is: " + timeOfConcurrentHashMap/1000000000.0f + " seconds");
    }
}
