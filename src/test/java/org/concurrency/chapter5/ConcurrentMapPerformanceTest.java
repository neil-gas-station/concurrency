package org.concurrency.chapter5;

import org.concurrency.chapter3.ImprovedHashMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentMapPerformanceTest {
    private final int THREAD_COUNT = 20;

    private final int ADD_VALUE_TIMES = 1000000;

    private final String KEY_PREFIX = "performanceTest";

    private final Map<Object, Object> synchronizedMap = Collections.synchronizedMap(new HashMap<>());
    private final Runnable  synchronizedMapTestTask= new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                synchronizedMap.put(KEY_PREFIX + i, i);
            }
        }
    };

    private final Map<Object, Object> concurrentHashMap = new ConcurrentHashMap<>();
    private final Runnable concurrentHashMapTestTask = new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                concurrentHashMap.put(KEY_PREFIX + i, i);
            }
        }
    };

    private final ImprovedHashMap<Object, Object> improvedHashMap = new ImprovedHashMap<>();
    private final Runnable improvedHashMapTestTask = new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                improvedHashMap.put(KEY_PREFIX + i, i);
            }
        }
    };

    @Test
    public void concurrentMapPerformance_whenAddValue1000000TimesWith20Threads() throws InterruptedException {

        TestHarness testHarness = new TestHarness();

        long timeOfSynchronizedMap = testHarness.timeTasks(THREAD_COUNT, synchronizedMapTestTask);
        System.out.println(String.format(
                "It takes %s Nano seconds to add %s times with %s threads concurrently using Synchronized HashMap.",
                timeOfSynchronizedMap,
                ADD_VALUE_TIMES,
                THREAD_COUNT));

        long timeOfConcurrentHashMap = testHarness.timeTasks(THREAD_COUNT, concurrentHashMapTestTask);
        System.out.println(String.format(
                "It takes %s Nano seconds to add %s times with %s threads concurrently using Concurrent HashMap.",
                timeOfConcurrentHashMap,
                ADD_VALUE_TIMES,
                THREAD_COUNT));

        long timeOfImprovedHashsMap = testHarness.timeTasks(THREAD_COUNT, improvedHashMapTestTask);
        System.out.println(String.format(
                "It takes %s Nano seconds to add %s times with %s threads concurrently using Improved HashMap.",
                timeOfImprovedHashsMap,
                ADD_VALUE_TIMES,
                THREAD_COUNT));


        Assertions.assertTrue(timeOfImprovedHashsMap > timeOfSynchronizedMap && timeOfSynchronizedMap > timeOfConcurrentHashMap);

    }
}
