package org.concurrency.chapter9;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class LockOrderingDeadlockTest {
    @Test
    public void shouldTriggerDeadlock_whenLocksAreUsedInCycle() throws ExecutionException, InterruptedException {
        LockOrderingDeadlock deadlock = new LockOrderingDeadlock();
        ExecutorService executer = Executors.newFixedThreadPool(2);
        Future<String> future1 = executer.submit(() -> {
            try {
                deadlock.leftRight();
                return "ok";
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        Future<String> future2 = executer.submit(() -> {
            try {
                deadlock.rightLeft();
                return "ok";
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.println("The first thread completed: " + future1.get());
        System.out.println("The second thread completed: " + future2.get());
    }
}
