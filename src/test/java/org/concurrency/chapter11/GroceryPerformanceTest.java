package org.concurrency.chapter11;

import org.concurrency.chapter11.grocery.ImprovedSynchronizedGrocery;
import org.concurrency.chapter11.grocery.SynchronizedGrocery;
import org.concurrency.chapter6.ImprovedTestHarness;
import org.junit.jupiter.api.Test;

import java.util.concurrent.BrokenBarrierException;

public class GroceryPerformanceTest {

    private static final String FRUIT_APPLE = "APPLE_";

    private static final String VEGETABLE_CABBAGE = "CABBAGE_" ;

    private static final int OPERATION_TIMES = 10000;

    private static final int THREAD_COUNT = 10;

    /**
     * Result: Time spent 2.3679113 seconds using 10 threads  when operate 10000 times with SynchronizedGrocery.
     * @throws InterruptedException
     * @throws BrokenBarrierException
     */
    @Test
    public void groceryPerformanceTest_whenUseSynchronizedGrocery() throws InterruptedException, BrokenBarrierException {
        ImprovedTestHarness testHarness = new ImprovedTestHarness();
        SynchronizedGrocery synchronizedGrocery = new SynchronizedGrocery();
        Runnable groceryOperationTask = new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<OPERATION_TIMES;i++) {
                    synchronizedGrocery.addFruit(i, FRUIT_APPLE + i);
                    synchronizedGrocery.addVegetable(i, VEGETABLE_CABBAGE + i);
                }
            }
        };
        long timeConsumed = testHarness.timeTasks(THREAD_COUNT, groceryOperationTask);

        System.out.println("Time spent " + timeConsumed/1000000000f + " seconds" +" using "+ THREAD_COUNT +" threads " + " when operate " + OPERATION_TIMES + " times with SynchronizedGrocery.");
    }

    /**
     * Result: Time spent 0.4580155 seconds using 10 threads  when  operate 10000 times with ImprovedSynchronizedGrocery.
     * @throws InterruptedException
     * @throws BrokenBarrierException
     */
    @Test
    public void groceryPerformanceTest_whenUseImprovedSynchronizedGrocery() throws InterruptedException, BrokenBarrierException {
        ImprovedTestHarness testHarness = new ImprovedTestHarness();
        ImprovedSynchronizedGrocery improvedSynchronizedGrocery = new ImprovedSynchronizedGrocery();
        Runnable groceryOperationTask = new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<OPERATION_TIMES;i++) {
                    improvedSynchronizedGrocery.addFruit(i, FRUIT_APPLE + i);
                    improvedSynchronizedGrocery.addVegetable(i, VEGETABLE_CABBAGE + i);
                }
            }
        };
        long timeConsumed = testHarness.timeTasks(THREAD_COUNT, groceryOperationTask);

        System.out.println("Time spent " + timeConsumed/1000000000f + " seconds " + "using "+ THREAD_COUNT +" threads " +" when  operate " + OPERATION_TIMES +" times with ImprovedSynchronizedGrocery.");
    }
}
