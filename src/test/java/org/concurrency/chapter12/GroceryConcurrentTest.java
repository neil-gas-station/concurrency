package org.concurrency.chapter12;

import org.concurrency.chapter11.grocery.Grocery;
import org.concurrency.chapter11.grocery.ImprovedSynchronizedGrocery;
import org.concurrency.chapter11.grocery.SynchronizedGrocery;

public class GroceryConcurrentTest extends JSR166TestCase{

    public void testImprovedGroceryWithMultipleThreads() {
        ImprovedSynchronizedGrocery improvedSynchronizedGrocery = new ImprovedSynchronizedGrocery();

        AddInRunnable runnable1 = new AddInRunnable(improvedSynchronizedGrocery);

        AddInRunnable runnable2 = new AddInRunnable(improvedSynchronizedGrocery);

       Thread t1 = newStartedThread(runnable1);
       Thread t2 = newStartedThread(runnable2);
       awaitTermination(t1);
       awaitTermination(t2);

       assertEquals(runnable1.executeTimes + runnable2.executeTimes, improvedSynchronizedGrocery.getFruitSize());
       assertEquals(runnable1.executeTimes + runnable2.executeTimes, improvedSynchronizedGrocery.getVegetableSize());
    }

    public void testSynchronizedGroceryWithMultipleThreads() {
        SynchronizedGrocery synchronizedGrocery = new SynchronizedGrocery();

        AddInRunnable runnable1 = new AddInRunnable(synchronizedGrocery);

        AddInRunnable runnable2 = new AddInRunnable(synchronizedGrocery);

        Thread t1 = newStartedThread(runnable1);
        Thread t2 = newStartedThread(runnable2);
        awaitTermination(t1);
        awaitTermination(t2);

        assertEquals(runnable1.executeTimes + runnable2.executeTimes, synchronizedGrocery.getFruitSize());
        assertEquals(runnable1.executeTimes + runnable2.executeTimes, synchronizedGrocery.getVegetableSize());
    }

    class AddInRunnable extends CheckedRunnable {
        private final Grocery grocery;
        public final int executeTimes = 1000;
        AddInRunnable(Grocery grocery) {
            this.grocery = grocery;
        }
        @Override
        protected void realRun() {
            for(int i=0;i<executeTimes;i++) {
                this.grocery.addFruit(i,"Fruit:" + i);
                this.grocery.addVegetable(i, "Vegetable:" + i);
            }
        }
    }
}


