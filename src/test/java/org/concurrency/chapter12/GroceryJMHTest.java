package org.concurrency.chapter12;

import org.concurrency.chapter11.grocery.ImprovedSynchronizedGrocery;
import org.concurrency.chapter11.grocery.SynchronizedGrocery;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 3, time = 1)
@Measurement(iterations = 5, time = 5)
@Threads(10)
@Fork(1)
@State(value = Scope.Benchmark)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class GroceryJMHTest {

    @Param(value = {"100", "1000", "10000"})
    private int addCount;

    @Benchmark
    public void testImprovedGroceryFruitAdd(Blackhole blackhole) {
        ImprovedSynchronizedGrocery improvedSynchronizedGrocery = new ImprovedSynchronizedGrocery();
        for(int i=0;i<this.addCount;i++) {
            improvedSynchronizedGrocery.addFruit(i, "Fruit" + i);
        }
        blackhole.consume(this.addCount);
    }

    @Benchmark
    public void testSynchronizedGroceryFruitAdd(Blackhole blackhole) {
        SynchronizedGrocery synchronizedGrocery = new SynchronizedGrocery();
        for(int i=0;i<this.addCount;i++) {
            synchronizedGrocery.addFruit(i, "Fruit" + i);
        }
        blackhole.consume(this.addCount);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(GroceryJMHTest.class.getSimpleName())
                .build();
        new Runner(opt).run();
    }
}
