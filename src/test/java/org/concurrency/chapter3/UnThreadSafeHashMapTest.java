package org.concurrency.chapter3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;

public class UnThreadSafeHashMapTest {
    private static final String KEY_PREFIX = "hashmap_key";

    @Test
    public void hashMapIsNotThreadSafe_whenOperateInMultipleThreads() throws InterruptedException, ExecutionException {
        HashMap unThreadSafeHashMap = new HashMap<String, Integer>();

        Callable<String> callableTask = () -> {
           for(int i=0;i<1000;i++) {
               unThreadSafeHashMap.put(KEY_PREFIX+i,i);
           }
           return "ok";
        };

        ExecutorService executor = Executors.newScheduledThreadPool(10);

        List tasks = new ArrayList<Callable>();
        for(int i=0;i<10;i++) {
            tasks.add(callableTask);
        }
        List<Future<String>> taskResults = executor.invokeAll(tasks);
        Thread.sleep(20000);
        for(int i=0;i<taskResults.size();i++) {
            taskResults.get(i).get();
        }

        System.out.println("The final expected HashMap size should be 1000, actual size is: " + unThreadSafeHashMap.size());
        Assertions.assertFalse(unThreadSafeHashMap.size()==1000);
    }
}
