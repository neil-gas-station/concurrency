package org.concurrency.chapter3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ThreadSafeHashMapTest {
    private static final String KEY_PREFIX = "hashmap_key";

    @Test
    public void improvedHashMapIsThreadSafe_whenOperateInMultipleThreads() throws InterruptedException, ExecutionException {
        ImprovedHashMap threadSafeHashMap = new ImprovedHashMap<String,Integer>();

        Callable<String> callableTask = () -> {
            for(int i=0;i<1000;i++) {
                threadSafeHashMap.put(KEY_PREFIX+i,i);
            }
            return "ok";
        };

        ExecutorService executor = Executors.newScheduledThreadPool(10);

        List tasks = new ArrayList<Callable>();
        for(int i=0;i<10;i++) {
            tasks.add(callableTask);
        }
        List<Future<String>> taskResults = executor.invokeAll(tasks);
        Thread.sleep(20000);
        for(int i=0;i<taskResults.size();i++) {
            taskResults.get(i).get();
        }

        System.out.println("The final expected HashMap size should be 1000, actual size is: " + threadSafeHashMap.size());
        Assertions.assertTrue(threadSafeHashMap.size()==1000);
    }
}
