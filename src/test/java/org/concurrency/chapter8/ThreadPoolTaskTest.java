package org.concurrency.chapter8;

import org.concurrency.chapter3.ImprovedHashMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ThreadPoolTaskTest {
    private final int THREAD_COUNT = 10;

    private final int ADD_VALUE_TIMES = 1000000;

    private final String KEY_PREFIX = "cancellableTaskTest";


    private final ImprovedHashMap<Object, Object> improvedHashMap = new ImprovedHashMap<>();
    private final Runnable improvedHashMapTestTask = new Runnable() {
        @Override
        public void run() {
            for(int i=0;i<ADD_VALUE_TIMES;i++) {
                improvedHashMap.put(KEY_PREFIX + i, i);
            }
        }
    };

    /**
     * If the initial thread size is less than 8, this test will fail, which means that initialize the core pool size threads is not a better choice.
     * While the initial thread size is more than 8, this test will success.
     * The out put when initial thread size is 8:
         * Thread pool initialized, pool size is: 0
         * The total time consumed is: 1.5722909 seconds if the core pool size is not initialized
         * Thread pool initialized, pool size is: 8
         * The total time consumed is: 1.5663826 seconds if the core pool size is initialized
     */
    @Test
    public void timeConsumedIsLess_whenCorePoolSizeIsInitializedBeforeHand() {

        ThreadPoolTestHarness testHarness = new ThreadPoolTestHarness();

        long timeNotIntialized = testHarness.timeTasks(THREAD_COUNT, 5,false,improvedHashMapTestTask);

        System.out.println("The total time consumed is: " + timeNotIntialized/1000000000.0f + " seconds if the core pool size is not initialized");

       // testHarness = new ThreadPoolTestHarness();
        long timeInitialized = testHarness.timeTasks(THREAD_COUNT, 5,true,improvedHashMapTestTask);

        System.out.println("The total time consumed is: " + timeInitialized/1000000000.0f + " seconds if the core pool size is initialized");

        Assertions.assertTrue(timeInitialized < timeNotIntialized);
    }
}
